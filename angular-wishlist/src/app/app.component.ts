import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'; 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-wishlist';
  time = new Observable(observer =>{
    setInterval(() =>observer.next(new Date().toString()), 1000);
  });
}
