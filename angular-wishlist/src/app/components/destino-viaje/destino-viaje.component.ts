import { Component, OnInit, Input, HostBinding, EventEmitter, Output} from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.models';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model';





@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destinos: DestinoViaje;
  @Input() position: number;
  @HostBinding('attr.class') cssClass='col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;
  constructor(private store: Store<AppState>) {
    this.clicked=new EventEmitter();

  }

  ngOnInit(): void {
  }
  ir() {
    this.clicked.emit(this.destinos);
    return false;
  }


  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destinos));
    return false;
  }
  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destinos));
    return false;

  }
}
