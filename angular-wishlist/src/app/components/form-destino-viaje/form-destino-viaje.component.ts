import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { DestinoViaje } from '../../models/destino-viaje.models';
import { from, fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged,switchMap} from 'rxjs/operators';
import { ajax, AjaxResponse} from 'rxjs/ajax';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})

export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud=3;
  searchResults: string[];
  constructor(private fb: FormBuilder) { 
    //inicializar
    this.onItemAdded = new EventEmitter();
    //vinculacion con tag html
    this.fg = this.fb.group({
      nombre: ['',Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['']
    });
    
    //observador de tipeo
    this.fg.valueChanges.subscribe((form: any) =>{
      console.log('cambio el formulario: ', form);
    })
  }

  ngOnInit(): void {
    const elemNombre= <HTMLInputElement>document.getElementById('nombre');
fromEvent(elemNombre, 'input') 
.pipe(
  map ((e:KeyboardEvent) =>(e.target as HTMLInputElement).value),
 filter(Text=> Text.length > 2),
debounceTime(200),
distinctUntilChanged(),
switchMap(() => ajax('/assets/datos.json'))
).subscribe(AjaxResponse => {
  console.log(AjaxResponse);
  console.log(AjaxResponse.response);
  this.searchResults=AjaxResponse.response;
});
  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }
  
  nombreValidator(control: FormControl): {[s:string]: boolean } {
    const length = control.value.toString().trim().length;
    if (length > 0 && length < 5) {
      return { invalidNombre: true };
    }
    return null;
  }
  
  
nombreValidatorParametrizable(minLong:number): ValidatorFn {
  return(control: FormControl): {[s:string]: boolean} | null =>{
    const length = control.value.toString().trim().length;
    if (length > 0 && length < minLong) {
      return { minLongNombre: true };
    }
    return null
  }
}

}
